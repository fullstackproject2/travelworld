import { TestBed } from '@angular/core/testing';

import { CustomerserviceService } from './customer.service';

describe('CustomerService', () => {
  let service: CustomerserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
