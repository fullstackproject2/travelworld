import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerserviceService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';
import { ReCaptcha2Component } from 'ngx-captcha';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = '';
  password: string = '';
  siteKey: string;
  @ViewChild('captchaElem') captchaElem!: ReCaptcha2Component;

  constructor(private router: Router, private service: CustomerserviceService,private toastr: ToastrService) {
    this.siteKey ='6Le_yqopAAAAAMypxtDsE-tcimxa1vNiv6n4EVTT'
  }

  ngOnInit(): void {
    
  }
  

  async loginSubmit(loginForm: any) {
    localStorage.setItem('email', loginForm.email);

    // Check if the captcha is not verified
  if (!this.captchaElem.reCaptchaApi?.getResponse()) {
    this.toastr.error('Please verify the captcha.');
    return;
  }

    if (loginForm.email === 'Admin' && loginForm.password === 'Admin') {
      this.service.setLoggedIn(true); // Set loggedIn status to true
      this.router.navigate(['home']);
    } else {
      try {
        const data = await this.service.customerLogin(loginForm.email, loginForm.password).toPromise();
        if (data) {
          this.service.setLoggedIn(true); // Set loggedIn status to true after successful login
          this.router.navigate(['home']);
          this.toastr.success('login successful'),{
          timeout:3000,
          positionClass:'toast-top-right',
          closeButton:true,
          progressBar:true}
        } else {
          alert('Invalid Credentials');
          this.router.navigate(['login']);
        }
      } catch (error) {
        console.error(error);
        this.toastr.error('Login failed. Please try again.'),{
          timeout:3000,
          positionClass:'toast-top-right',
          closeButton:true,
          progressBar:true
        }
      }
    }
  }
  
  logout(): void {
    this.service.setLoggedIn(false); // Clear authentication status and navigate to login page
    this.router.navigate(['/login']);
  }
}