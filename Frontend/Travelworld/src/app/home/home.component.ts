import { Component, OnInit } from '@angular/core';
import axios from 'axios';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  destinations: { name: string, imageUrl: string, description: string, rating: number }[] = [];
  today: Date = new Date();
  activeSlideIndex: number = 0;
  isLoading: boolean = true;
  exploreIndiaDestinations: { name: string, imageUrl: string }[] = [];

  constructor() { }

  ngOnInit() {
    this.fetchDestinationImages();
  }

  fetchDestinationImages() {
    const apiKey = '9PPEoWbkTupNlCveggU1eOHspto4dhyZR74A2YCTLNLaYgfq2bF8bNDE';
    const destinations = [
      { name: 'Paris', description: 'The capital city of France known for its art, culture, and landmarks.', rating: 4.5 },
      { name: 'New York', description: 'The city that never sleeps, famous for its skyscrapers, Broadway shows, and diverse culture.', rating: 4.8 },
      { name: 'Tokyo', description: 'The bustling metropolis of Japan, blending traditional culture with modern technology.', rating: 4.7 },
      { name: 'London', description: 'The historic city of the United Kingdom, home to landmarks such as the Big Ben and Buckingham Palace.', rating: 4.3 },
      { name: 'Rome', description: 'The ancient city of Italy, known for its rich history, art, and architecture, including the Colosseum and Vatican City.', rating: 4.6 },
      { name: 'Sydney', description: 'A vibrant city in Australia, famous for its iconic Sydney Opera House, Harbour Bridge, and beautiful beaches.', rating: 4.9 },
      // Add more destinations as needed
    ];

    Promise.all(destinations.map(destination =>
      axios.get(`https://api.pexels.com/v1/search?query=${destination.name}&per_page=1`, {
        headers: {
          Authorization: apiKey
        }
      })
        .then(response => {
          const photo = response.data.photos[0];
          return {
            name: destination.name,
            imageUrl: photo ? photo.src.large2x : '', // Use original-sized image for highest quality
            description: destination.description,
            rating: destination.rating
          };
        })
        .catch(error => {
          console.error('Error fetching image:', error);
          return null;
        })
    ))
      .then(images => {
        this.destinations = images.filter(image => image !== null) as { name: string, imageUrl: string, description: string, rating: number }[];
        this.isLoading = false;
      })
      .catch(error => {
        console.error('Error fetching images:', error);
        this.isLoading = false;
      });

    const indiaDestinations = [
      'New Delhi',
      'Bangalore',
      'Mumbai',
      'Hyderabad',
      'Varanasi'
    ];

    Promise.all(indiaDestinations.map(destination =>
      axios.get(`https://api.pexels.com/v1/search?query=${destination}&per_page=1`, {
        headers: {
          Authorization: apiKey
        }
      })
        .then(response => {
          const photo = response.data.photos[0];
          return {
            name: destination,
            imageUrl: photo ? photo.src.large2x : '' // Use original-sized image for highest quality
          };
        })
        .catch(error => {
          console.error('Error fetching image:', error);
          return null;
        })
    ))
      .then(images => {
        this.exploreIndiaDestinations = images.filter(image => image !== null) as { name: string, imageUrl: string }[];
        this.isLoading = false;
      })
      .catch(error => {
        console.error('Error fetching images:', error);
        this.isLoading = false;
      });
  }

  nextSlide() {
    this.activeSlideIndex = (this.activeSlideIndex + 1) % this.destinations.length;
  }

  prevSlide() {
    this.activeSlideIndex = this.activeSlideIndex === 0 ? this.destinations.length - 1 : this.activeSlideIndex - 1;
  }
}
