import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  email: string = '';
  errorMessage: string = '';

  constructor(private authService: AuthService) {}

  forgotPassword(): void {
    this.authService.forgotPassword(this.email).subscribe(
      (response) => {
        // Handle success
        console.log('Password reset email sent successfully.');
      },
      (error: any) => {
        // Handle error
        this.errorMessage = 'An error occurred. Please try again later.';
        console.error(error);
      }
    );
  }
}
