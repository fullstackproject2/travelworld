  import { Component, OnInit } from '@angular/core';
  import { Router } from '@angular/router';
  import { CustomerserviceService } from '../customer.service';
  import { ToastrService } from 'ngx-toastr';

  @Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
  })
  export class RegisterComponent implements OnInit {

    fullName: string = '';
    email: string = '';
    password: string = '';
    phoneNumber: string = '';
    aadharNumber: string = ''; // Changed from licenseNumber
    address: string = '';
    confirmPassword: string = '';
    constructor(private customerService: CustomerserviceService, private router: Router,private toastr: ToastrService) {}

    ngOnInit(): void {
      // No initialization code here
    }

    customerRegister() {
      if (this.password === this.confirmPassword) {
        const customerData = {
          fullName: this.fullName,
          email: this.email,
          password: this.password,
          phoneNumber: this.phoneNumber,
          aadharNumber: this.aadharNumber,
          address: this.address
        };
    
        this.customerService.addCustomer(customerData).subscribe({
          next: (response: any) => {
            this.toastr.success('Registration successful', '', {
              timeOut: 3000,
              positionClass: 'toast-top-right',
              closeButton: true,
              progressBar: true
            });
            this.router.navigate(['/login']);
          },
          error: (error: any) => {
            this.toastr.error('Registration failed', '', {
              timeOut: 3000,
              positionClass: 'toast-top-right',
              closeButton: true,
              progressBar: true
            });
            console.error('Registration failed', error);
            // Show error message to the user
          }
        });
      } else {
        alert("Passwords do not match!");
      }
    }
  }    