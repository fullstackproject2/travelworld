import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CustomerserviceService } from './customer.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private customerService: CustomerserviceService, private router: Router) {}

  canActivate(): boolean {
    if (this.customerService.isLoggedIn()) {
      return true; 
    } else {
      this.router.navigate(['/login']); 
      return false; 
    }
  }
}
