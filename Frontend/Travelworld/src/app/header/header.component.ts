// header.component.ts
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  authenticated: boolean = false;
  isUserLogged: any;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    // Subscribe to the authentication state
    this.authService.isAuthenticated$().subscribe(authenticated => {
      this.authenticated = authenticated;
    });
  }

  logout() {
    // Clear authentication status and navigate to login page
    this.authService.setAuthenticated(false);
    // Additional logout logic if needed
  }
}
