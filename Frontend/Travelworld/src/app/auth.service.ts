import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {}

  // Method to get the current authentication state
  isAuthenticated$() {
    return this.isAuthenticated.asObservable();
  }

  // Method to update the authentication state
  setAuthenticated(value: boolean) {
    this.isAuthenticated.next(value);
  }

  // Method to handle forgot password functionality
  forgotPassword(email: string): Observable<any> {
    return this.http.post<any>('/api/forgot-password', { email });
  }
}
