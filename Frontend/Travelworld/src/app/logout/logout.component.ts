import { Component } from '@angular/core';
import { CustomerserviceService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'] // <-- Corrected property name
})
export class LogoutComponent {
  constructor(private router: Router, private service: CustomerserviceService) {
    alert('Logged Out Successfully');
    this.service.isUserLoggedOut();

    localStorage.removeItem('emailId');
    localStorage.clear();

    this.router.navigate(['login']);
  }
}
