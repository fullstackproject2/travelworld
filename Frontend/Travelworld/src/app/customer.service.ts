import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerserviceService {
  private baseUrl = 'http://localhost:8085'; 
  private loggedInStatus: boolean = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  constructor(private http: HttpClient) { }

  customerLogin(email: string, password: string): Observable<any> {
    const body = { email, password };
    return this.http.post<any>(`${this.baseUrl}/loginCustomer`, body).pipe(
      tap(() => this.setLoggedIn(true)),
      catchError(this.handleError<any>('customerLogin'))
    );
  }

  addCustomer(customerData: { fullName: string; email: string; password: string; phoneNumber: string; aadharNumber: string; address: string; }): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/addCustomer`, customerData).pipe(
      catchError(this.handleError<any>('addCustomer'))
    );
  }

  isLoggedIn(): boolean {
    return this.loggedInStatus;
  }

  isUserLoggedOut() {
    this.setLoggedIn(false);
    localStorage.removeItem('loggedIn');
  }

  // Make setLoggedIn method public
  setLoggedIn(value: boolean) {
    this.loggedInStatus = value;
    localStorage.setItem('loggedIn', JSON.stringify(value));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      return throwError(error.message);
    };
  }
}
