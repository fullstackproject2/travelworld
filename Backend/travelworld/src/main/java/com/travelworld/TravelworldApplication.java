package com.travelworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;

@EntityScan(basePackages="com.model")
@SpringBootApplication(scanBasePackages="com")
@EnableJpaRepositories(basePackages = "com.dao")
@CrossOrigin(origins = "http://localhost:4200")
public class TravelworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelworldApplication.class, args);
	}

}