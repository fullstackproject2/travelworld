package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dao.DestinationDao;
import com.model.Destination;

import java.util.List;

@RestController
@RequestMapping("/destinations")
public class DestinationController {

    @Autowired
    private DestinationDao destinationDao;

    @GetMapping
    public ResponseEntity<List<Destination>> getAllDestinations() {
        List<Destination> destinations = destinationDao.getAllDestinations();
        return ResponseEntity.ok().body(destinations);
    }

    @GetMapping("/{destinationId}")
    public ResponseEntity<Destination> getDestinationById(@PathVariable Long destinationId) {
        Destination destination = destinationDao.getDestinationById(destinationId);
        if (destination != null) {
            return ResponseEntity.ok().body(destination);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Destination> addDestination(@RequestBody Destination destination) {
        Destination savedDestination = destinationDao.addDestination(destination);
        return ResponseEntity.ok().body(savedDestination);
    }

    @PutMapping("/{destinationId}")
    public ResponseEntity<Destination> updateDestination(@PathVariable Long destinationId, @RequestBody Destination destination) {
        destination.setId(destinationId); // Using setId instead of setDestinationId
        Destination updatedDestination = destinationDao.updateDestination(destination);
        return ResponseEntity.ok().body(updatedDestination);
    }

    @DeleteMapping("/{destinationId}")
    public ResponseEntity<Void> deleteDestinationById(@PathVariable Long destinationId) {
        destinationDao.deleteDestinationById(destinationId);
        return ResponseEntity.ok().build();
    }
}
