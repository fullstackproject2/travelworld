package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Destination;

import java.util.List;

@Service
public class DestinationDao {

    @Autowired
    private DestinationRepository destinationRepository;

    public List<Destination> getAllDestinations() {
        return destinationRepository.findAll();
    }

    public Destination getDestinationById(Long destinationId) {
        return destinationRepository.findById(destinationId).orElse(null);
    }

    public List<Destination> getDestinationsByName(String destinationName) {
        return destinationRepository.findByDestinationName(destinationName);
    }

    public Destination addDestination(Destination destination) {
        return destinationRepository.save(destination);
    }

    public Destination updateDestination(Destination destination) {
        return destinationRepository.save(destination);
    }

    public void deleteDestinationById(Long destinationId) {
        destinationRepository.deleteById(destinationId);
    }
}
